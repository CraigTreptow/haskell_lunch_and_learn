module Pal where

import Data.Char

-- What a palindrome is --

isPalindrome :: String -> Maybe Bool
isPalindrome string = isOwnReverse (normalize string)

normalize :: String -> String
normalize string = map toLower (filter isLetter string)

isOwnReverse :: String -> Maybe Bool
isOwnReverse word =
  case word of
    [] -> Nothing
    _ -> Just (word == reverse word)

