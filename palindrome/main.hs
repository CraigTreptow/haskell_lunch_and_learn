import Pal (isPalindrome)

-- The interactive program --

main :: IO ()
main =
  do
    line <- getLine
    print (verbose line)

verbose :: String -> String
verbose line =
  case (isPalindrome line) of
    Nothing -> "Please enter a word or phrase."
    Just False -> "Sorry, not a palindrome."
    Just True -> "Yup, it's palindrome!"

