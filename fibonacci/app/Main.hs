module Main where

import Lib
import Text.Printf

main :: IO ()
main = do
    putStrLn "Enter a number:"
    l <- getLine
    let x = read l :: Integer
    let f = Lib.fib(x)
    printf "Fibonacci of %d is %s\n\n" (x) (show f)
