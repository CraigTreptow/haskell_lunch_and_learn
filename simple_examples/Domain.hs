module Domain where
-- contact = { first_name: 'Craig', middle_initial: nil, last_name: 'Treptow', email: 'craig.treptow@gmail.com', verified_email: True }

data PersonalName = PersonalName { firstName :: String
                                 , middleName :: Maybe String
                                 , lastName :: String
                                 } deriving (Show)

data Email = Email String deriving (Show)

data VerifiedEmail = VerifiedEmail String deriving (Show)

data EmailAddress =   MakeEmail Email
                    | MakeVerifiedEmail VerifiedEmail
                    deriving (Show)

data Contact = Contact { name :: PersonalName
                       , email :: EmailAddress
                       } deriving (Show)

main = do
  let name = PersonalName { firstName = "Craig", middleName = Nothing, lastName = "Treptow" }
  let e = newEmail "craig.treptowgmail.com"
  let ee = Email "craig.treptowgmail.com"
  let v = verifyEmail e
  putStrLn (show ee)
  -- putStrLn (show e)
  -- putStrLn (show v)
  let vMail = verifyEmail $ Email "craig.treptow@gmail.com"
  let craig = Contact { name = name
                      , email = MakeVerifiedEmail vMail}

  putStrLn (show craig)

-- helper functions --

newEmail :: String -> Email
newEmail s | elem '@' s = Email s
           | otherwise = error "Invalid email string"

verifyEmail :: Email -> VerifiedEmail
verifyEmail (Email e) = VerifiedEmail e