module Simple3 where

-- list comprehensions

-- predicate function used below
isEven n = n `mod` 2 == 0

a = [x * 3 | x <- [1..10]]
aa = map (* 3) [1..10]

b = [x * 3 | x <- [1..10], isEven x]
bb = filter isEven (map (* 3) [1..10])

main :: IO ()
main = do
  putStrLn $ show a
  putStrLn $ show aa
  putStrLn $ show b
  putStrLn $ show bb
