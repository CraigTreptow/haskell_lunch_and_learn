module Simple2 where

-- partial application
add x y = x + y
addTwo = add 2

multiply x y = x * y
multiplyTwo = multiply 2

-- higher order functions (HOF)
processNumbers f x y = f x y

applyTwice :: (a -> a) -> a -> a
-- applyTwice f x = f (f x)

main :: IO ()
main = do
  -- partial application
  putStrLn $ show (add 1 2)                         -- 3
  putStrLn $ show (addTwo 2)                        -- 4
  putStrLn $ show (addTwo 100)                      -- 102

  -- HOF
  putStrLn $ show (processNumbers add 100 200)      -- 300
  putStrLn $ show (processNumbers multiply 100 200) -- 20000

  putStrLn $ show (applyTwice (+3) 10)              -- 16
