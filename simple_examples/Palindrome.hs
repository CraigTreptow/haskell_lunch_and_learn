module Palindrome where

import Data.Char

isOwnReverse :: String -> Bool
isOwnReverse word = word == reverse word

normalize :: String -> String
normalize word = filter isLetter (map toLower word)

isPalindrome :: String -> Maybe Bool
isPalindrome maybeWord =
  case maybeWord of
    [] -> Nothing
    _  -> Just (isOwnReverse $ (normalize maybeWord))

check :: String -> String
check word =
  case (isPalindrome word) of
    Nothing    -> "Please enter a word."
    Just False -> "Sorry, this word is not a palindrome."
    Just True  -> "Congratulations, this word is a palindrome."

main :: IO ()
main = do
  word <- getLine
  print $ check word
