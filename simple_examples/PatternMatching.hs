module PatternMatching where

-- Using recursion (with the "if/then/else" expression)
factorial1 :: Int -> Int
factorial1 n = if n < 2
               then 1
               else n * factorial1 (n - 1)

-- Using recursion (with pattern matching)
factorial2 :: Int -> Int
factorial2 0 = 1
factorial2 n = n * factorial2 (n - 1)

craig n =
  case n of
    0 -> "Craig"
    1 -> "Treptow"
    _ -> "WAT"


main :: IO ()
main = do
  putStrLn $ "Fac1: " ++ show(factorial1 3)
  putStrLn $ "Fac1: " ++ show(factorial1 3)
  putStrLn $ "Fac2: " ++ show(factorial2 3)
