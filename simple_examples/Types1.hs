module Types1 where

-- aliases
type SSN = String

-- type inference
showSSN :: SSN -> String
showSSN ssn =
  if ((length ssn) == 9)
  then a ++ "-" ++ b ++ "-" ++ c
  else "invalid SSN"
    where
      a = take 3 ssn
      b = take 2 (drop 3 ssn)
      c = take 4 (drop 5 ssn)

-- bounded integer
-- (minBound, maxBound) :: (Int, Int)
factorialBI:: Int -> Int
factorialBI 0 = 1
factorialBI n = n * factorialBI (n - 1)

-- unbounded integer
-- (minBound, maxBound) :: (Integer, Integer)
factorialUBI :: Integer -> Integer
factorialUBI 0 = 1
factorialUBI n = n * factorialUBI (n - 1)

-- integral contains both Int and Integer (only whole numbers)
-- (minBound, maxBound) :: (Integral, Integral)
factorialI :: (Integral a) => a -> a
factorialI 0 = 1
factorialI n = n * factorialI (n - 1)

main :: IO ()
main = do
  let ssn = "111223333"
  putStrLn $ show (showSSN ssn)
  putStrLn $ show (showSSN "some string")

  putStrLn $ show (factorialBI 50)            -- too big for Int, wraps
  putStrLn $ show (factorialUBI 50)           -- no issue for Integer
  putStrLn $ show (factorialI 50 :: Int)      -- no issue for Integral
  putStrLn $ show (factorialI 50 :: Integer)  -- no issue for Integer

