module Simple1 where
import Text.Printf

infiniteList = [1 .. ]

format s = printf "length of %s is %d\n" (show s) (length s)

x = [1,2,3]
-- x = reverse x
y = reverse x 

main :: IO ()
main = do
  putStrLn "Hello, World!"

  format x
  format y

  -- laziness
  putStrLn (show (take 5 infiniteList))             -- [1,2,3,4,5]

  -- $ is syntacic sugar to get rid of some parentheses
  putStrLn $ show (take 5 infiniteList)             -- [1,2,3,4,5]

