isAMultipleOf :: Int -> Int -> Bool
isAMultipleOf i multiplier = i `mod` multiplier == 0

fizzBuzz :: Int -> String
fizzBuzz i | i `isAMultipleOf` 3 && i `isAMultipleOf` 5 = "FizzBuzz"
fizzBuzz i | i `isAMultipleOf` 3 = "Fizz"
fizzBuzz i | i `isAMultipleOf` 5 = "Buzz"
fizzBuzz i = show i

main :: IO ()
main = do
  -- first example uses list comphrehension
  -- [ f x | x <- someList ] <==> map f someList
  mapM_ putStrLn [ fizzBuzz n | n <- [1..10] ]
  mapM_ putStrLn $ map fizzBuzz [1..10]
