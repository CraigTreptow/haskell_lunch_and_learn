module Types where

type Name = String
type Email = String
type Phone = String

-- Using record syntax to create the type let
-- Haskell automatically create the name, age, and email functions
data Person = Person { name :: Name
                     , email :: Email
                     } deriving (Show)

data Customer = Customer { person :: Person
                         , phone :: Phone
                         } deriving (Show)

data Lead = Prospect Person | Active Customer 

sayHelloToPerson :: Person -> String
sayHelloToPerson l = "You are just a person " ++ name l

sayHelloToCustomer :: Customer -> String
sayHelloToCustomer c = "Thanks for the business " ++ name (person c)

sayHello :: Lead -> String
sayHello (Prospect person) = "Have a good day " ++ name person
sayHello (Active customer) = "You rock, " ++ name (person customer)

craig = Person { name = "Craig Treptow", email = "craig.treptow@hey.com" }
customer = Customer { person = craig, phone = "123-123-1234" }

-- lead = Prospect craig
-- sayHello lead
--
-- agent = Active customer
-- sayHello agent
