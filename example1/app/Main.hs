module Main where

import Lib

-- interact takes a function and produces IO
-- main = interact $ show . sum . map read . words
-- $ operator
-- f1 (f2 (f3 (f4))) = f1 $ f2 $f3 $f4
-- . operator (functional composition)
-- f: X -> Y and g: Y -> Z
-- . allows them to be composed to f: X -> Z
-- words takes a string and outputs a list of strings
-- read converts a string to a number type
-- (map read $ words "1 2") :: Int
-- sum $ map read $ words "1 2"
-- we need a function to pass interact, though, so
-- show sum . map . read . words "1 2"

main :: IO ()
main = interact $ show . sum . map read . word

-- imperative not declarative
-- main = do
--   val1 <- readLn
--   val2 <- readLn
--   let sum = solveMeFirst val1 val2
--   print sum
