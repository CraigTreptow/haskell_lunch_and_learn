<!---
  pandoc --standalone --incremental --css style.css --to slidy --output haskell.html haskell.md
--->

<meta name="duration" content="55" />

---
title: "Haskell Lunch & Learn"
author: Craig Treptow
date: June 25, 2020
output: slidy_presentation
---

# Agenda
- Caveats
- Why learn Haskell?
- What is it?
- Who uses it?
- History
- Lambda Calculus
- Questions you may have already
- Examples

# Caveats
- I am not here to convince you of anything
- I had started learning Haskell when Mike asked about L&L's
  - I had walked away, but have started again
  - Need time for concepts to begin to sink in
- I have barely scratched the surface
- My only hope is that you will consider checking out Haskell

# Why Learn Haskell?
- Anecdotal evidence:
  - Developers say they are a better programmer because of it
  - Developers say they can avoid runtime errors
    - They can happen, but it is harder
    - This is what attracted me
  - Developers say it makes refactoring easier
    - This can be done with TDD (type driven development)
    - Chris Toomey (formerly Thoughtbot) has talked about it more than once
      - He's mostly into Elm and GraphQL
- Stimulate the mind:
  - It is good to see different ways of doing things
    - Many smart people don't work in your daily language
  - It forces a different way of thinking
    - Specifically Haskell, since you cannot fall back to OOP (unlike Scala)
  - It exposes you to different ideas

# What is Haskell?
- Named after Haskell Curry
    - American mathematician & logician
- General Purpose
  - Useful for any programming task
- Statically Typed
  - Mostly about timing
  - Early as possible (static)
  - Late as possible (dynamic)

# What is Haskell?, Cont.
- Purely Functional
  - No generally accepted definition
  - Usually:
    - Functions only rely on their arguments
    - Immutable data
    - Higher Order Functions (first class)
      - Can pass functions around as values
- Lazy
  - Delays evaluating expressions as late as possible

# Who uses Haskell?
- A very small sampling:
- Compilers
  - Microsoft, Github
- Finance
  - JP Morgan, Deutsche Bank, LexisNexis Risk Solutions, Barclays Capital, Bank of America
- Advertising/SPAM filtering
  - Facebook
- E-Commerce/Supply Chain
  - Target
- Manufacturing
  - Tesla
- Also multiple entries in security, blockchain, software, consulting, etc.

# History
- Educators in the 1980's wanted:
  - To consolidate existing FP languages
  - A language to provide the basis for more R&D
- Designed by a committee
- Biggest names involved:
  - Simon Peyton Jones
  - Philip Wadler
  - Erik Meijer
- Influenced by Miranda (and other FP languages)
  - Miranda was commercially owned, not free for all
- First released in 1990
- Current release is Haskell 2010
- Haskell 2020 is coming

# Lambda Calculus?
- A method of computation
- Haskell is a lambda calculus
- 11th letter in Greek alphabet (λ)
- Alonzo Church (1930's)
  - Wanted to investigate foundations of math
- John McCarthy
  - Invented lisp (1958)
  - First to see the connection to programming
- A model of calculations that *only* use functions
- Examples:
  - λx.x - identity (always returns it's argument)
  - (λx.x)y - identity applied to function y
  - (λx.y) - function that always returns y

# Examples
- NeoVIM/GHCi

# More information
- Slides & Code (https://gitlab.com/CraigTreptow/haskell_lunch_and_learn)
- Haskell home page (https://www.haskell.org)
- Hoogle: Haskell API search engine (https://hoogle.haskell.org)
- Hackage: Central package archive (http://hackage.haskell.org)
- Books
  - Get Programming with Haskell (https://www.manning.com/books/get-programming-with-haskell)
  - Haskell Programming (https://haskellbook.com)
  - Learn You a Haskell (http://learnyouahaskell.com)
  - A Type of Programming (https://atypeofprogramming.com)
  - Real World Haskell (http://book.realworldhaskell.org)
- Online guides
  - https://www.fpcomplete.com/haskell/learn/
  - https://www.schoolofhaskell.com/user/school/starting-with-haskell
  - https://mmhaskell.com/liftoff

# The End

