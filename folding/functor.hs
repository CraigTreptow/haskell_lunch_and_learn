-- association list
database :: [(Integer, String)]
database = [(1, "Julie")
           ,(2, "Chris")
           ,(3, "Alonzo")
           ,(4, "Melman")]


greetUser :: Integer -> Maybe String
greetUser record =
  -- type hole to let compiler tell us what we can write
  -- _ ("Hello, " ++) (lookup record database)

  mapToMaybe ("Hello, " ++) (lookup record database)

--            one function is provide   one funtion is returned
-- mapToMaybe :: (String -> String) -> (Maybe String -> Maybe String)
mapToMaybe :: (a -> b) -> (Maybe a -> Maybe b)
mapToMaybe func Nothing = Nothing
mapToMaybe func (Just string) = Just (func string)

generalMap :: (a -> b) -> f a -> f b
generalMap func Nothing = Nothing
