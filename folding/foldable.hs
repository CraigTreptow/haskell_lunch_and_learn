import Data.Foldable
import Data.Monoid

-- Foldable
example1 :: Maybe String
example1 = Nothing

example2 :: Maybe String
example2 = Just "Alonzo"

-- Monoid
-- Sum 3 <> Sum 4
-- Product 3 <> Product 4
-- getSum (Sum 3 <> Sum 4)
-- getProduct (Product 3 <> Product 4)
