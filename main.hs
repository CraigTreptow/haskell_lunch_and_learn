import Data.Char

isPalindrome :: String -> Bool
isPalindrome word = word == reverse word

nonemptyPal :: String -> Maybe Bool
nonemptyPal word =
  case word of
    [] -> Nothing
    _  -> Just (isPalindrome word)

verbose :: String -> String
verbose word =
  let w = allLowerCase (filter isLetter word) in
  case (nonemptyPal w) of
    Nothing    -> "Please enter a word."
    Just False -> "Sorry, that is not a palindrome"
    Just True  -> "Yay, it's a palindrome!"

allLowerCase :: String -> String
allLowerCase word = map toLower word

main :: IO ()
main = 
  do 
    word <- getLine
    print (verbose word)
