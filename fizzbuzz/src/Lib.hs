module Lib ( fizzBuzz ) where

isAMultipleOf :: Integral a => a -> a -> Bool
isAMultipleOf i multiplier = i `mod` multiplier == 0

fizzBuzz :: (Integral a, Show a) => a -> String
fizzBuzz i | i `isAMultipleOf` 3 && i `isAMultipleOf` 5 = "FizzBuzz"
fizzBuzz i | i `isAMultipleOf` 3 = "Fizz"
fizzBuzz i | i `isAMultipleOf` 5 = "Buzz"
fizzBuzz i = show i
