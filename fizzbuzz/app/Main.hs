module Main where

import Lib as L

main :: IO ()
main = mapM_ putStrLn [ L.fizzBuzz n | n <- [1..100] ]
