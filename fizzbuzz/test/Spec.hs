import Test.Hspec
import Lib as L
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

main :: IO ()
main = hspec $ do
  describe "fizzbuzz" $ do
      it "is Fizz when multiple of 3" $ do
        L.fizzBuzz 3 `shouldBe` "Fizz"

      it "is Buzz when multiple of 5" $ do
        L.fizzBuzz 5 `shouldBe` "Buzz"

      it "is FizzBuzz when multiple of 3 and 5" $ do
        L.fizzBuzz 15 `shouldBe` "FizzBuzz"

      it "is the number when not multiple of 3 or 5" $ do
        L.fizzBuzz 2 `shouldBe` "2"

